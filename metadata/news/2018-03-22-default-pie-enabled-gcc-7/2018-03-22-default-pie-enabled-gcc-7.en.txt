Title: GCC 7 has been unmasked
Author: Rasmus Thomsen <cogitri@exherbo.org>
Content-Type: text/plain
Posted: 2018-03-22
Revision: 3
News-Item-Format: 1.0

GCC 7 was released a while ago and is deemed stable enough for
general consumption. It brings important changes and it is
recommended to closely read this news item. After reading this
news item you should consider rebuilding all of your packages
to avoid eventual build failures later on and to benefit from
the numerous changes and improvements GCC 7 introduces.

=== PIE by default ===

Since sys-devel/gcc-7.3.0 GCC will build position-independent
executables (PIE) by default. This increases the effectiveness
of (user space) ASLR greatly. Do note that PIE comes with a
performance penalty (which is negligible on AMD64 but may impact
other arches heavily).

This change may cause build failures, such as:

"relocation R_X86_64_32 against '[SYMBOL]' can not be used when making a shared object; recompile with -fPIC"

This usually just requires a plain rebuild of the package
providing the static library. For example if sys-apps/paludis fails
to compile with the following error:

"/usr/x86_64-pc-linux-gnu/bin/x86_64-pc-linux-gnu-ld: error:
/usr/x86_64-pc-linux-gnu/lib/libgtest.a(gtest-all.cc.o):
requires dynamic R_X86_64_PC32 reloc against '_Znwm' which may overflow at runtime; recompile with -fPIC"

you have to rebuild dev-cpp/gtest. Paludis' build should succeed afterwards.

Some packages which contain ASM code that isn't position independent
can't be compiled with PIE. You may still compile these packages by
adding "-no-pie" to your LDFLAGS. This should only be done if
nothing else works (and on a per-package-basis), since this limits
(user space) ASLR for this package considerably.

=== New libstdc++ abi ===

With 7.3.0 we also enabled the newer ABI for libstdc++ for all available GCC
slots. This can cause linking errors (containing something with 'cxx11') when
a package and one of its dependents use mixed ABIs. It's enough to rebuild
the dependency indicated by the error message. For example if a package fails
with:

"undefined reference to `Json::Value::Value(std::__cxx11::basic_string<char,
std::char_traits<char>, std::allocator<char> > const&)"

you need to rebuild dev-libs/jsoncpp and the build of the original package
should succeed afterwards.

=== Spectre mitigation ===

GCC 7.3.0 also introduces initial retpoline support. Retpoline is a mitigation
for the Spectre vulnerability. You can install `sys-apps/spectre-meltdown-checker`
to check if your CPU is vulnerable to Spectre (and Meltdown):

    `# cave resolve -x sys-apps/spectre-meltdown-checker`
    `# spectre-meltdown-checker`

If you're vulnerable to Spectre (Variant 1 and/or 2), you most likely want to enable
CONFIG_RETPOLINE in your kernel configuration to enable Spectre mitigations for your kernel.
If you want to enable them for user space applications as well you can do so
by adding `-mindirect-branch=thunk` to your C{,XX}FLAGS. Do note that
these mitigations come with some rather hefty performance penalties for some applications
though, especially in terms of raw throughput. As mentioned the performance degradation
differs greatly between applications, most applications (e.g. games, day-to-day tasks)
basically don't have any degradation at all, but other applications like redis have
a performance penalty of up to 35% compared to running it without retpolines.
You can use `-mindirect-branch=thunk` on a per-package basis if you don't
want these performance penalties across all of your applications.

If you're vulnerable to Meltdown (Variant 3, exclusive to Intel) you most likely
want to upgrade to at least Linux 4.4.110/4.9.75/4.14.12. These are the first
versions to include the complete KPTI (Kernel Page-Table Isolation) patchset.
You should also update your `firmware/intel-microcode` package to the latest
available version. KPTI basically works by completely separating kernel space
and user space. This comes with yet another performance penalty, especially
for applications which do lots of syscalls (e.g. for lots of small I/O transfers)
and the likes. Once again the impact strongly differs between applications,
e.g. PostgreSQL loses nearly 20% of its performance, while redis only has a
performance degradation of about 6% and other programs might have negligible
degradation if any.
