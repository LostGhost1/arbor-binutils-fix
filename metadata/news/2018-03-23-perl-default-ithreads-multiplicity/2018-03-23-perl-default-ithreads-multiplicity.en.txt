Title: ithreads and multiplicity enabled by default for dev-lang/perl
Author: Benedikt Morbach <moben@exherbo.org>
Content-Type: text/plain
Posted: 2018-03-23
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: dev-lang/perl[-ithreads]

The [ithreads] and [multiplicity] options for dev-lang/perl have been removed,
enabling these features by default. If you had the ithreads option disabled, you
will need to rebuild all installed perl modules after updating dev-lang/perl
using the following steps

Update perl:

    # cave resolve -1 dev-lang/perl -x

Make sure the latest version of dev-lang/perl is used system-wide (e.g. 5.26):

    # eclectic perl set 5.26

Rebuild all perl modules:

    # perl_slot=5.26
    # cave resolve -1 -Cs $(cave print-owners -m "*/*::installed[.INSTALLED_TIME<$(cave print-id-metadata --raw-name INSTALLED_TIME --format '%v' dev-lang/perl:${perl_slot}::installed)]" -f '%c/%p:%s\n' /usr/$(readlink /usr/host)/lib/perl5/) -E dev-perl/Scalar-List-Utils -x

You might have to re-run the last cave invocation if any exheres fail to build
due to their dependencies not being rebuilt yet.
