# Copyright 2015 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gcc.gnu.org
require toolchain-runtime-libraries

export_exlib_phases src_unpack src_prepare src_configure src_install

SUMMARY="GNU Atomic Library"

LICENCES="GPL-2"
SLOT="$(ever major)"

MYOPTIONS=""

DEPENDENCIES="
    build:
        sys-devel/binutils[>=2.20.1] [[ note = [ minimal version of binutils for LTO ] ]]
        sys-devel/gcc:${SLOT}
"

if [[ ${PV} == *_pre* ]] ; then
    ECONF_SOURCE="${WORKBASE}/gcc-$(ever major)-${PV##*_pre}/libatomic"
    WORK="${WORKBASE}/gcc-$(ever major)-${PV##*_pre}/build/$(exhost --target)/libatomic"
elif [[ ${PV} == *-rc* ]] ; then
    ECONF_SOURCE="${WORKBASE}/gcc-${PV/rc/RC-}/libatomic"
    WORK="${WORKBASE}/gcc-${PV/rc/RC-}/build/$(exhost --target)/libatomic"
else
    ECONF_SOURCE="${WORKBASE}/gcc-${PV/_p?(re)/-}/libatomic"
    WORK="${WORKBASE}/gcc-${PV/_p?(re)/-}/build/$(exhost --target)/libatomic"
fi

# Those tests previously only ran during gcc's test_expensive phase, however
# they fail completely now:  "... unexpected failures: 22 ..." (4.9.2)
RESTRICT="test"

# TODO(compnerd) find a way to expose build tools when cross-compiling and replace
# {AR,RANLIB}_FOR_BUILD accordingly
DEFAULT_SRC_CONFIGURE_PARAMS=(
    AR_FOR_BUILD=$(exhost --build)-ar
    CC_FOR_BUILD=$(exhost --build)-gcc-${SLOT}
    LD_FOR_BUILD=$(exhost --build)-ld
    CPP_FOR_BUILD=$(exhost --build)-gcc-cpp-${SLOT}
    RANLIB_FOR_BUILD=$(exhost --build)-ranlib
    AS_FOR_TARGET=$(exhost --tool-prefix)as
    CC_FOR_TARGET=$(exhost --tool-prefix)gcc-${SLOT}
    LD_FOR_TARGET=$(exhost --tool-prefix)ld
    # Dependency tracking breaks the build, GCC developers argue that it shouldn't be disabled, see
    # upstream discussion at https://gcc.gnu.org/bugzilla/show_bug.cgi?id=55930
    --hates=disable-dependency-tracking

    --disable-multilib
)

libatomic_src_unpack() {
    default
    edo mkdir -p "${WORK}"
}

libatomic_src_prepare() {
    edo cd "${ECONF_SOURCE}/.."
    default
}

libatomic_src_configure() {
    CC=$(exhost --tool-prefix)gcc-${SLOT}       \
    CPP=$(exhost --tool-prefix)gcc-cpp-${SLOT}  \
    CXX=$(exhost --tool-prefix)g++-${SLOT}      \
    default
}
libatomic_src_install() {
    default

    symlink_dynamic_libs ${PN}
    slot_dynamic_libs ${PN}
    slot_other_libs ${PN}.{a,la}

    nonfatal edo rmdir "${IMAGE}"/usr/$(exhost --target)/lib
}

