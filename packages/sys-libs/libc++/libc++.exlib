# Copyright 2019 Marc-Antoine Perennou <keruspe@exherbo.org>
# Copyright 2011 Elias Pipping <pipping@exherbo.org>
# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

if ever at_least 11; then
    require llvm-project [ pn="libcxx" check_target="check-cxx" asserts=true rtlib=true ]
else
    require llvm-project [ pn="libcxx" check_target="check-libcxx" asserts=true rtlib=true ]
fi

require toolchain-funcs

export_exlib_phases src_test

SUMMARY="A new implementation of the C++ standard library"

if ever at_least 11; then
    MYOPTIONS+="
        pstl [[ description = [ Enable parallel algorithms through pstl ] ]]
    "
fi

MYOPTIONS+="
    ( libc: musl )
"

DEPENDENCIES="
    build+run:
        sys-libs/libc++abi[providers:compiler-rt?][providers:libgcc?]
"

if ever at_least 11; then
    DEPENDENCIES+="
        build+run:
            pstl? ( dev-cpp/pstl )
    "
fi

if ever at_least 12; then
    :
else
    DEFAULT_SRC_PREPARE_PATCHES=(
        "${FILES}"/libc++-9-Fix-make_shared-for-types-with-noexcept-false-dtor.patch
    )
fi

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DLIBCXX_CXX_ABI:STRING="libcxxabi"
    -DLIBCXX_CXX_ABI_INCLUDE_PATHS:PATH="/usr/$(exhost --target)/include/libc++abi"
    -DLIBCXX_ENABLE_EXCEPTIONS:BOOL=ON
    -DLIBCXX_ENABLE_RTTI:BOOL=ON
    -DLIBCXX_ENABLE_SHARED:BOOL=ON
    -DLIBCXX_ENABLE_STATIC:BOOL=ON
    -DLIBCXX_ENABLE_STATIC_ABI_LIBRARY:BOOL=OFF
    -DLIBCXX_ENABLE_THREADS:BOOL=ON
    -DLIBCXX_ENABLE_WERROR:BOOL=OFF
    -DLIBCXX_INCLUDE_TESTS:BOOL=ON
    -DLIBCXX_INSTALL_HEADERS:BOOL=ON
    -DLIBCXX_INSTALL_LIBRARY:BOOL=ON
    -DLIBCXX_INSTALL_SHARED_LIBRARY:BOOL=ON
    -DLIBCXX_INSTALL_STATIC_LIBRARY:BOOL=ON
    -DLIBCXX_STATICALLY_LINK_ABI_IN_STATIC_LIBRARY:BOOL=ON
)

CMAKE_SRC_CONFIGURE_OPTIONS+=(
    'asserts LIBCXX_ENABLE_ASSERTIONS'
    'libc:musl LIBCXX_HAS_MUSL_LIBC'
    'providers:compiler-rt LIBCXX_USE_COMPILER_RT'
    'providers:compiler-rt LIBCXXABI_USE_LLVM_UNWINDER'
)

if ever at_least 11; then
    CMAKE_SRC_CONFIGURE_OPTIONS+=(
        'pstl LIBCXX_ENABLE_PARALLEL_ALGORITHMS'
    )
fi

libc++_src_test() {
    # If GCC is used remove some tests that fail with GCC >=10
    if cxx-is-gcc; then
        edo rm "${CMAKE_SOURCE}"/test/std/utilities/meta/meta.unary/meta.unary.prop/is_constructible.pass.cpp
        edo rm "${CMAKE_SOURCE}"/test/std/utilities/tuple/tuple.tuple/tuple.cnstr/deduct.pass.cpp
        edo rm "${CMAKE_SOURCE}"/test/std/utilities/tuple/tuple.tuple/tuple.creation/tuple_cat.pass.cpp
    fi

    # Automagic test based on the presence of gdb, but fails if Python bindings are missing
    ever at_least 12 && edo rm "${CMAKE_SOURCE}"/test/libcxx/gdb/gdb_pretty_printer_test.sh.cpp

    esandbox allow_net "unix:${WORK}/test/filesystem/Output/dynamic_env/test.*/socket"
    esandbox allow_net "unix:/tmp/test.*/socket"
    esandbox allow_net "unix:/tmp/status.pass.cpp.dir/static_env.1/socket"
    esandbox allow_net "unix:/tmp/symlink_status.pass.cpp.dir/static_env.1/socket"

    llvm-project_src_test

    esandbox disallow_net "unix:${WORK}/test/filesystem/Output/dynamic_env/test.*/socket"
    esandbox disallow_net "unix:/tmp/test.*/socket"
    esandbox disallow_net "unix:/tmp/status.pass.cpp.dir/static_env.1/socket"
    esandbox disallow_net "unix:/tmp/symlink_status.pass.cpp.dir/static_env.1/socket"
}

