# Copyright 2008, 2009, 2013, 2014 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'tig-0.9.1.ebuild' from Gentoo, which is:
#     Copyright 1999-2007 Gentoo Foundation

require bash-completion zsh-completion
require github [ user=jonas release=${PNV} suffix=tar.gz ]

export_exlib_phases src_install

SUMMARY="Text mode interface for git"
HOMEPAGE="https://jonas.github.io/${PN}/"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS=""

# requires access to /dev/tty
RESTRICT="test"

DEPENDENCIES="
    build:
        app-doc/asciidoc[>=8.4]
        virtual/pkg-config
    build+run:
        sys-libs/ncurses
        sys-libs/readline:=[>=6.3]
    run:
        dev-scm/git[>=1.5.4]
"

DEFAULT_SRC_TEST_PARAMS=( -j1 )

DEFAULT_SRC_INSTALL_PARAMS=( install-doc )

# Same file is used for both completions
BASH_COMPLETIONS=( "contrib/${PN}-completion.bash" )
ZSH_COMPLETIONS=( "contrib/${PN}-completion.bash _${PN}" )

tig_src_install() {
    default
    dobin contrib/tig-pick

    docinto examples
    dodoc contrib/*.tigrc

    bash-completion_src_install
    zsh-completion_src_install
}

